vim.g.mapleader = " "

local opts = { noremap = true, silent = false }

vim.keymap.set("n", "<leader>e", vim.cmd.Ex, opts)
vim.keymap.set("n", "<leader>w", vim.cmd.write, opts)

vim.keymap.set("n", "H", "g^", opts)
vim.keymap.set("n", "L", "g$", opts)
vim.keymap.set("n", "j", "gj", opts)
vim.keymap.set("n", "k", "gk", opts)

vim.keymap.set("n", "<C-j>", "<C-w><C-j>", opts)
vim.keymap.set("n", "<C-k>", "<C-w><C-k>", opts)
vim.keymap.set("n", "<C-h>", "<C-w><C-h>", opts)
vim.keymap.set("n", "<C-l>", "<C-w><C-l>", opts)

vim.keymap.set("n", "<leader><leader>", "<C-^>", opts)
vim.keymap.set("n", "<silent><Right>", ":bn<cr>", opts)

vim.keymap.set("n", "<Left>", ":bprev<cr>", opts)
vim.keymap.set("n", "<Right>", ":bnext<cr>", opts)

vim.keymap.set("i", "jj", "<Esc>", opts)

vim.keymap.set("t", "<C-j>", "<C-j><C-\\><C-n><C-w><C-j>", opts)
vim.keymap.set("t", "<C-k>", "<C-j><C-\\><C-n><C-w><C-k>", opts)
vim.keymap.set("t", "<C-h>", "<C-j><C-\\><C-n><C-w><C-h>", opts)
vim.keymap.set("t", "<C-l>", "<C-j><C-\\><C-n><C-w><C-l>", opts)

-- Setup a keymap to reload our init.lua to make testing changes much easier.
vim.keymap.set("n", "<leader><cr>", "<cmd>lua ReloadConfig()<cr>", opts)

local function toggle_colorcolumn ()
    local colorcolumn = vim.opt.colorcolumn:get()
    local new_value = ''

    if #colorcolumn == 0 or colorcolumn[1] == '' then
        new_value = '100'
    end

    for _, win in ipairs(vim.api.nvim_tabpage_list_wins(0)) do
        vim.api.nvim_win_set_option(win, 'colorcolumn', new_value)
    end
end

vim.keymap.set("n", "<leader>cc", toggle_colorcolumn, opts)

local function toggle_hlsearch()
    vim.opt.hlsearch = not vim.opt.hlsearch:get()
end

vim.keymap.set("n", "<leader>h", toggle_hlsearch, opts)
