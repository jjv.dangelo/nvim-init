-- Install Packer if it hasn't been installed yet
local ensure_packer = function()
    local fn = vim.fn
    local install_path = fn.stdpath('data') .. '/site/pack/packers/start/packer.nvim'

    if fn.empty(fn.glob(install_path)) > 0 then
        fn.system({
            'git',
            'clone',
            '--depth',
            '1',
            'https://github.com/wbthomason/packer.nvim',
            install_path,
        })

        vim.cmd([[packadd packer.nvim]])
        return true
    end

    return false
end

local packer_bootstrap = ensure_packer()

-- Only required if you have packer configured as `opt`
vim.cmd [[packadd packer.nvim]]

require('packer').startup(function(use)
    use 'wbthomason/packer.nvim'

    use {
        'nvim-telescope/telescope.nvim',
        tag = '0.1.x',
        requires = { { 'nvim-lua/plenary.nvim' } },
    }

    use {
        'nvim-telescope/telescope-fzf-native.nvim',
        run = 'cmake -S. -Bbuild -DCMAKE_BUILD_TYPE=Release && cmake --build build --config Release && cmake --install build --prefix build'
    }

    use {
        'nvim-treesitter/nvim-treesitter', 
        run = ':TSUpdate',
    }

    use {
        'VonHeikemen/lsp-zero.nvim',
        branch = 'v3.x',
        requires = {
            { 'williamboman/mason.nvim' },
            { 'williamboman/mason-lspconfig.nvim' },

            { 'neovim/nvim-lspconfig' },
            { 'hrsh7th/nvim-cmp' },
            { 'hrsh7th/cmp-nvim-lsp' },
            { 'L3MON4D3/LuaSnip' },
        },
    }

    use 'simrat39/rust-tools.nvim'

    use {
        'olimorris/persisted.nvim',
        config = function()
            require('persisted').setup({
                use_git_branch = true,
                default_branch = 'master',
                autoload = true,
                on_autoload_no_session = function()
                    print("Starting a new session")
                    vim.cmd.SessionStart()
                end
            })
        end,
    }

    use {
        'windwp/nvim-autopairs',
        event = 'InsertEnter',
        config = function()
            require('nvim-autopairs').setup {}
        end,
    }

    -- colorschemes
    use {
        'shaunsingh/nord.nvim',
        config = function()
            vim.g.nord_italic = false
            vim.g.nord_bold = false
            vim.g.nord_borders = true
        end,
    }

    use {
        'olivercederborg/poimandres.nvim',
        config = function()
            require('poimandres').setup {
                dim_nc_background = true,
                disable_italics = true,
            }
        end,
    }
end)

-- First run will install Packer and the configured plugins
if packer_bootstrap then
    require('packer').sync()
    return
end
