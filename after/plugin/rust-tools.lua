local function on_attach(client, buffer)
end

local extraArgs = {
    '--',
    '-D', 'clippy::missing_safety_doc',
}

local opts = {
    tools = {
        runnables = {
            use_telescope = true,
        },

        inlay_hints = {
            auto = true,
            show_parameter_hints = false,
            parameter_hints_prefix = '',
            other_hints_prefix = '',
        },
    },

    server = {
        on_attach = on_attach,

        settings = {
            ['rust-analyzer'] = {
                check = {
                    features = 'all',
                    command = 'clippy',
                    extraArgs = extraArgs,
                },
            },
        },
    },
}

require('rust-tools').setup(opts)
