local lsp = require('lsp-zero')

lsp.preset('recommended')
require('mason').setup()

require('mason-lspconfig').setup {
    ensure_installed = {
        'clangd',
        'omnisharp',
        'cssls',
        'glsl_analyzer',
        'rust_analyzer',
        'golangci_lint_ls',
        'html',
        'tsserver',
        'lua_ls',
        'markdown_oxide',
        'powershell_es',
        'zls',
    },
}

local config = require('lspconfig')
config.rust_analyzer.setup {}

config.zls.setup {}

local cmp = require('cmp')

local cmp_select = { behavior = cmp.SelectBehavior.Select }
cmp.setup({
    sources = {
        { name = 'path' },
        { name = 'nvim_lsp', keyword_length = 2 },
        { name = 'buffer', keyword_length = 2 },
        { name = 'vsnip', keyword_length = 2 },
    },

    window = {
        completion = cmp.config.window.bordered(),
        documentation = cmp.config.window.bordered(),
    },

    mapping = {
        ['<S-Tab>'] = cmp.mapping.select_prev_item(cmp_select),
        ['<Tab>'] =   cmp.mapping.select_next_item(cmp_select),
        ['<C-.>'] =   cmp.mapping.complete(),
        -- ['<C-u>'] =   cmp.mapping.scroll_docs(4),
        -- ['<C-d>'] =   cmp.mapping.scroll_docs(-4),
        ['<Esc>'] =   cmp.mapping.close(),
        ['<CR>'] =    cmp.mapping.confirm({
            select = true,
            behavior = cmp.ConfirmBehavior.Insert,
        }),
    },
})

lsp.set_preferences({
    sign_icons = { }
})

lsp.on_attach(function(client, buffer)
    local opts = { buffer = bufnr, remap = false }

    local diag_float_grp = vim.api.nvim_create_augroup('DiagnosticFloat', { clear = true })
    vim.api.nvim_create_autocmd('CursorHold', {
        callback = function()
            vim.diagnostic.open_float(nil, { focusable = false })
        end,

        group = diag_float_grp,
    })

    local format_sync_grp = vim.api.nvim_create_augroup('Format', {})
    vim.api.nvim_create_autocmd('BufWritePre', {
        callback = function()
            vim.lsp.buf.format({ timeout_ms = 200 })
        end,

        group = format_sync_grp,
    })

    local call_safe = function(f)
        if f then
            f()
        end
    end

    local function diagnostic_next()
        if vim.lsp.diagnostic.goto_next then
            vim.lsp.diagnostic.goto_next()
        end
    end

    vim.wo.signcolumn = 'yes'

    vim.keymap.set("n", "gd", vim.lsp.buf.definition, opts)
    vim.keymap.set("n", "ga", vim.lsp.buf.code_action, opts)
    vim.keymap.set("n", "gi", vim.lsp.buf.implementation, opts)

    vim.keymap.set("n", "K",           vim.lsp.buf.hover, opts)
    vim.keymap.set("n", "<leader>vws", vim.lsp.buf.workspace_symbol, opts)
    vim.keymap.set("n", "<leader>vd",  function()
        call_safe(vim.lsp.diagnostic.open_float())
    end, opts)

    -- vim.keymap.set("n", "[d", function() call_safe(vim.lsp.diagnostic.goto_prev) end, opts)
    -- vim.keymap.set("n", "]d", function() call_safe(vim.lsp.diagnostic.goto_next) end, opts)

    vim.keymap.set("n", "<leader>h",   vim.lsp.buf.signature_help, opts)
    -- vim.keymap.set("n", "<leader>vca", vim.lsp.buf.code_action, opts)
    -- vim.keymap.set("n", "<leader>vrr", vim.lsp.buf.references, opts)

    -- Setup keys that are similar to Visual Studio
    vim.keymap.set("n", "<F12>", vim.lsp.buf.definition, opts)
    vim.keymap.set("n", "<C-F12>", vim.lsp.buf.declaration, opts)
    vim.keymap.set("n", "<S-F12>",   vim.lsp.buf.references, opts)
    vim.keymap.set("n", "<C-S-F12>",   diagnostic_next, opts)
    vim.keymap.set("n", "<C-r><C-r>",  vim.lsp.buf.rename, opts)
end)

lsp.setup()
