--vim.cmd.colorscheme("poimandres")

local colors = {
    bg = "#32302f",
    bg_inactive = "#2c2a28",
    fg = "#d5c4a1",
    comment = "#928374",
    keyword = "#b8a878",
    string = "#a89984",
    func = "#bdae93",
    variable = "#d5c4a1",
    constant = "#a88760",
    type = "#a88760",
    netrw_dir_link = "#ffd83b",
    line = "#46413e",
    statusline_sel = "#46413e",
    statusline_unsel = "#2b2a28",
}

vim.cmd("highlight clear")
vim.cmd("syntax reset")

vim.o.background = "dark"
vim.o.termguicolors = true

local highlights = {
    Normal = { fg = colors.fg, bg = colors.bg },
    Comment = { fg = colors.comment },
    Keyword = { fg = colors.keyword },
    String = { fg = colors.string },
    Function = { fg = colors.func },
    Variable = { fg = colors.variable },
    Constant = { fg = colors.constant },
    Type = { fg = colors.type },
    -- Additional highlights
    Statement = { fg = colors.keyword },
    Identifier = { fg = colors.variable },
    PreProc = { fg = colors.keyword },
    Special = { fg = colors.constant },
    Todo = { fg = colors.constant, bg = colors.comment, bold = true },
    LineNr = { fg = colors.comment },
    CursorLineNr = { fg = colors.fg, bg = colors.line },
    CursorLine = { bg = colors.line }, -- slightly lighter background for cursor line
    Visual = { bg = "#504945" }, -- visual mode selection
    Directory = { fg = colors.netrw_dir_link }, -- highlight for netrw directories
    NetrwDir = { fg = colors.netrw_dir_link }, -- netrw directory highlight
    NetrwClassify = { fg = colors.netrw_dir_link }, -- netrw classification highlight
    NetrwLink = { fg = colors.netrw_dir_link }, -- netrw link highlight
    NetrwSymLink = { fg = colors.netrw_dir_link }, -- netrw symlink highlight
    StatusLine = { fg = colors.fg, bg = colors.line },
    StatusLineNC = { fg = colors.fg, bg = colors.statusline_unsel },
    ActiveWindow = { bg = colors.bg },
    InactiveWindow = { bg = colors.bg_inactive },
}

for group, opts in pairs(highlights) do
    vim.api.nvim_set_hl(0, group, opts)
end

vim.cmd('highlight ActiveWindow guibg=' .. colors.bg)
vim.cmd('highlight InactiveWindow guibg=' .. colors.bg_inactive)

vim.cmd([[
    augroup WindowManagement
      autocmd!
      autocmd WinEnter * call Handle_Win_Enter()
      autocmd FocusLost * call Handle_Focus_Lost()
      autocmd FocusGained * call Handle_Focus_Gained()
    augroup END

    function! Handle_Win_Enter()
      setlocal winhighlight=Normal:ActiveWindow,NormalNC:InactiveWindow
    endfunction

    function! Handle_Focus_Lost()
      setlocal winhighlight=Normal:InactiveWindow,NormalNC:InactiveWindow
    endfunction

    function! Handle_Focus_Gained()
      setlocal winhighlight=Normal:ActiveWindow,NormalNC:InactiveWindow
    endfunction
]])
